<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HackClinic extends \Eloquent
{
    protected $table = 'hack_clinics';
    public $timestamps = true;

    public function doctors(){
    	return $this->hasMany('App\HackDoctor', 'clinic_id', 'id');
    }

    public function panels(){
    	return $this->hasMany('App\HackPanel', 'clinic_id', 'id');
    }
}
