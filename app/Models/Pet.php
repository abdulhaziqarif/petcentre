<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model 
{

    protected $table = 'pets';
    public $timestamps = true;

    public function owner(){
    	return $this->hasOne('App\User','id','user_id');
    }

}