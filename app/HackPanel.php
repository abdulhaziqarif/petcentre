<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HackPanel extends \Eloquent
{
    protected $table = 'hack_panels';
    public $timestamps = true;
}
