<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $inputs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $config = \DB::select('SELECT * FROM sys_config LIMIT 1');

        return $this->from($config[0]->official_mail)
                    ->view('mail.forgot_password')
                    ->subject('MAGICX - DAPATKAN KEMBALI KATALALUAN')
                    ->with([
                               'forgot_link' => $this->inputs['forgot_link']
                           ]);
    }
}
