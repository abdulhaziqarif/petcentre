<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Activation extends Mailable
{
    use Queueable, SerializesModels;

    protected $inputs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // $config = \DB::select('SELECT * FROM sys_config LIMIT 1');

        return $this->from('petcentre@petcentre.com')
                    ->view('email.activation')
                    ->subject('Welcome to Pet Centre')
                    ->with([
                               'activation_link' => $this->inputs['activation_link']
                           ]);
    }
}
