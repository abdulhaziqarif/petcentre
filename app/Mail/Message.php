<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Message extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('petcentre@petcentre.com')
                    ->view('email.message')
                    ->subject('Information from Pet Centre')
                    ->with([
                               'content' => $this->inputs['content'],
                               'username' => $this->inputs['username'],
                               'ads' => $this->inputs['ads'],
                           ]);
    }
}
