<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HackDoctor extends \Eloquent
{
    protected $table = 'hack_doctors';
    public $timestamps = true;
}
