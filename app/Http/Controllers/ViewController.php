<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clinic;
use App\Models\Shop;
use App\Models\Shelter;
use App\Models\Profile;

use App\User;
use Auth;


class ViewController extends Controller
{
    public function show_login(){
    	return view('login');
    }

    public function show_register(){
		return view('register');
    }

    public function show_index(){
    	return view('master');
    }

    public function show_dashboard(){
    	$data = [
    				'user' => \App\User::count(),
                    'clinic' => \App\Models\Clinic::count(),
                    'shelter' => \App\Models\Shelter::count(),
                    'shop' => \App\Models\Shop::count(),
                    'pet' => \App\Models\Pet::count(),
                    'item' => \App\Models\Item::count(),
                    'service' => \App\Models\Service::count(),
                    'recent_user' => \App\User::orderBy('created_at','desc')->get(),
                    'active_user' => \App\User::where('status',1)->count()
    			];

        return \View::make('dashboard',$data);
    }

    public function show_profile($test){
        // dd($id);
        $clinic = Clinic::where('user_id',$test)->first();
        // dd($clinic);
        $shelter = Shelter::where('user_id',$test)->first();
        $shop = Shop::where('user_id',$test)->first();
        $profile = Profile::where('user_id',$test)->first();

        if($clinic != null){
            $user = User::select('users.*','users.id as user_id','a.*','a.id as clinic_id')
                        ->join('clinics as a','users.id','=','a.user_id')
                        ->where('users.id',$test)
                        ->first();
        }
        elseif ($shelter != null) {
            $user = User::select('users.*','users.id as user_id','a.*','a.id as shelter_id')
                        ->join('shelters as a','users.id','=','a.user_id')
                        ->where('users.id',$test)
                        ->first();
        }
        elseif ($shop != null) {
            $user = User::select('users.*','users.id as user_id','a.*','a.id as shops_id')
                        ->join('shops as a','users.id','=','a.user_id')
                        ->where('users.id',$test)
                        ->first();
        }
        else{
            $user = User::select('users.*','users.id as user_id','a.*','a.id as profile_id')
                        ->join('profile as a','users.id','=','a.user_id')
                        ->where('users.id',$test)
                        ->first();
        }
        // dd($user);
        return \View::make('user.profile',compact('user'));
    }

    public function updateprofile(Request $r){
        $user = User::where('id',$r->id)->first();
        // dd($r);
        $image = request()->file('image');
        if($image != null){
            $user_id = Auth::user()->id;
            $ext = $image->extension(); 
            $imagepath = $r->username.$user_id.".".$ext;  
            $image->move(public_path().'/assets/img/profile',$imagepath);
            $path = "assets/img/profile/".$imagepath;
            $url =  \URL::to($path);
        }else{
            $url = $user->photo_url;
          }

        if($r->clinic_id != null){
            $clinic = Clinic::where('user_id',$user->id)->first();
            $clinic->name = $r->company_name;
            $clinic->address = $r->address;
            $clinic->town = $r->town;
            $clinic->poscode = $r->poscode;
            $clinic->state = $r->state;
            $clinic->country = $r->country;
            $clinic->phone = $r->phone;
            $clinic->latitude = $r->latitude;
            $clinic->longitude = $r->longitude;
            $clinic->hour_from = $r->hour_from;
            $clinic->hour_until = $r->hour_until;
            $clinic->photo = $url;
            $clinic->save();
        }
        elseif ($r->shelter_id != null) {
           $shelter = Shelter::where('user_id',$user->id)->first();
            $shelter->name = $r->company_name;
            $shelter->address = $r->address;
            $shelter->town = $r->town;
            $shelter->poscode = $r->poscode;
            $shelter->state = $r->state;
            $shelter->country = $r->country;
            $shelter->phone = $r->phone;
            $shelter->latitude = $r->latitude;
            $shelter->longitude = $r->longitude;
            $shelter->hour_from = $r->hour_from;
            $shelter->hour_until = $r->hour_until;
            $shelter->photo = $url;
            $shelter->save();
        }
        elseif ($r->shop_id != null) {
            $clinic = Shop::where('user_id',$user->id)->first();
            $clinic->name = $r->company_name;
            $clinic->address = $r->address;
            $clinic->town = $r->town;
            $clinic->poscode = $r->poscode;
            $clinic->state = $r->state;
            $clinic->country = $r->country;
            $clinic->phone = $r->phone;
            $clinic->latitude = $r->latitude;
            $clinic->longitude = $r->longitude;
            $clinic->hour_from = $r->hour_from;
            $clinic->hour_until = $r->hour_until;
            $clinic->photo = $url;
            $clinic->save();
        }
        else{
            $profile = Profile::where('user_id',$user->id)->first();
            $profile->address = $r->address;
            $profile->town = $r->town;
            $profile->poscode = $r->poscode;
            $profile->state = $r->state;
            $profile->country = $r->country;
            $profile->phone = $r->phone;
            $profile->photo = $url;
            $profile->save();
        }

        $user->email = $r->email;
        $user->username = $r->username;
        $user->photo_url = $url;
        $user->save();

        return redirect()->route('profile',$r->id);
    }

}
