<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use App\Models\Pet;
use App\Models\Clinic;
use App\Models\Shelter;
use App\Models\Profile;

use App\Mail\Activation;

use Validator;
use Auth;

class APIAuthController extends Controller
{
    public function login(Request $r){
    header("Access-Control-Allow-Origin: *");

    $user = User::where('email',$r->email)->first();

        if($user!=null){

            if($user->status == 1){
                if(Auth::attempt(['email' => $r->email, 'password' => $r->password])){              
                    return response()->json(
                    [
                        'message'   =>  'Success',
                        'status'    =>  '200',
                        'data'      =>  Auth::user()
                    ],200);
                Auth::logout();
            }else{
                    return response()->json(
                    [
                        'message'   =>  'The combination email and password is not in our database',
                        'status'    =>  '404',
                        'data'      =>  null,
                    ],404);
            }     
            }
            else{
                return response()->json(
                    [
                        'message'   =>  'Please check your email and activate to proceed',
                        'status'    =>  '404',
                        'data'      =>  null,
                    ],404);
            }
              
        }
        else if($user == null){
            return response()->json(
                    [
                        'message'   =>  'The email has not been registered',
                        'status'    =>  '404',
                        'data'      =>   null,
                    ],404);
        }
        
    }

    public function register(Request $r){
        header("Access-Control-Allow-Origin: *");
        $validation = Validator::make($r->all(),[
            'email'     =>  'unique:users',
            'username'  =>  'required',
            'password'  =>  'required',
            'roles'     =>  'required'
         ]);

         if($validation->fails()){
            return response()->json(
                    [
                        'message'   =>  'Error',
                        'status'    =>  '404',
                        'data'      =>  $validation->errors(),
                    ],404);
         }

        $user           = new User();
        $user->email    = $r->email;
        $user->username = $r->username;
        $user->password = bcrypt($r->password);
        $user->photo_url= $r->photo_url;
        $user->roles    = $r->roles;
        $user->status   = 0;
        $user->save();

        

        if($r->roles == 'seller'){
            $profile          = new Profile();
            $profile->user_id = $user->id;
            $profile->save();
        }
        else if($r->roles == 'clinic'){
            $profile          = new Clinic();
            $profile->user_id = $user->id;
            $profile->save();
        }
        else if ($r->roles == 'shelter'){
            $profile          = new Shelter();
            $profile->user_id = $user->id;
            $profile->save();
        }
        else if ($r->role == 'shop'){
            $profile          = new Shop();
            $profile->user_id = $user->id;
            $profile->save();
        }
        else{
            return response()->json(
                    [
                        'message'   =>  'Please select role',
                        'status'    =>  '404',
                        'data'      =>  null,
                    ],404);
        }


        $link = \URL::to('activate/' . $user->id);

        $data = [
            'user_id'           => $user->id,
            'email'             => $user->email,
            'activation_link'   => $link
        ];

        \Mail::to($user->email)->send(new Activation($data));

        return response()->json(
        [
            'message'   =>  'Please check your email and activate your account',
            'status'    =>  '200',
            'data'      =>  $user,

        ],200);
    }
}
