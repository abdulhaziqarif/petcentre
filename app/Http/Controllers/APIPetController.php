<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use App\Models\Pet;
use App\Models\Clinic;
use App\Models\Shelter;
use App\Models\Profile;

use App\Mail\Activation;

use Validator;
use Auth;

class APIPetController extends Controller
{
     public function getAllPet(){
        header("Access-Control-Allow-Origin: *");
        $pet = Pet::select('pets.*','b.username')
                    ->join('users as b', 'pets.user_id','=','b.id')
                    ->get();

        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$pet,

        ],200);
    }

    public function getPetDetail($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Pet::select('pets.*','b.username')
                    ->join('users as b', 'pets.user_id','=','b.id')
                    ->where('pets.id',$id)
                    ->first();
        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$pet,

        ],200);
    }

     public function listPetById($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Pet::select('pets.*','b.username')
                    ->join('users as b', 'pets.user_id','=','b.id')
                    ->where('user_id',$id)
                    ->get();
        return response()->json(
        [
            'message'   =>  'Success',
            'status'    =>  '200',
            'data'      =>  $pet,

        ],200);
    }

    public function addPetById(Request $r){
        header("Access-Control-Allow-Origin: *");

        $validation = Validator::make($r->all(),[
            'photo'     =>  'required',
         ]);

         if($validation->fails()){
            return response()->json(
                    [
                        'message'   =>  'Error',
                        'status'    =>  '404',
                        'data'      =>  $validation->errors(),
                    ],404);
         }

        $image      = request()->file('photo');

        if($image!=null){
            $ext        = $image->extension(); 
            $imagepath  = $r->name.$r->user_id.".".$ext;  
            $image->move(public_path().'/assets/img/pet/',$imagepath);
            $path       = "assets/img/pet/".$imagepath;
            $url        =  \URL::to($path);
        }
        else{
            $url = null;
        }
        
        // DECODE BASE 64
        // $img = explode(",", $image)[1];
        // $base64data = str_replace(',', '', $img); 

        // $imagedata = base64_decode($base64data);
        // $path       = "assets/img/pet/".$imagepath;
        // file_put_contents($path,$imagedata);
        
        $pet                = new Pet();
        $pet->name          = $r->name;
        $pet->price         = $r->price;
        $pet->description   = $r->description;
        $pet->type          = $r->type;
        $pet->user_id       = $r->user_id;
        $pet->photo         = $url;

        if($r->year != null){
        $pet->year          = $r->year;
        }else{
        $pet->year          = 0;            
        }

        if($r->month != null){
        $pet->month         = $r->month;
        }else{
        $pet->month         = 0;            
        }

        $pet->save();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }

    public function searchpet(Request $r){
        // $pet = Pet::where('')
    }

    public function delete($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Pet::where('id',$id)
                ->first();
        $file = $pet->photo;

        // /File::delete($file);

        return response()->json(
        [
            'message'   =>  'Success',
            'status'    =>  '200',
            'data'      =>  $pet,

        ],200);
    }

    public function update(Request $r){
        header("Access-Control-Allow-Origin: *");

        $pet    = Pet::where('id',$r->id)->first();

        $image  = request()->file('photo');

        if($image!=null){
            $ext        = $image->extension(); 
            $imagepath  = $r->name.$r->user_id.".".$ext;  
            $image->move(public_path().'/assets/img/pet/',$imagepath);
            $path       = "assets/img/pet/".$imagepath;
            $url        =  \URL::to($path);
        }
        else{
            $url = $pet->photo;
        }
        
        $pet->name          = $r->name;
        $pet->price         = $r->price;
        $pet->description   = $r->description;
        $pet->type          = $r->type;
        $pet->photo         = $url;
        $pet->month         = $r->month;
        $pet->year          = $r->year;            
        $pet->save();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }

    public function search(Request $r){
        header("Access-Control-Allow-Origin: *");

        $pet = Pet::select('pets.*','b.username')
                    ->join('users as b', 'pets.user_id','=','b.id')
                    ->where('name', 'like', '%'.$r->name.'%')
                    ->where('price', '<=', $r->price)
                    ->where('type', $r->type)
                    ->get();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }
}
