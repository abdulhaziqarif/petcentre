<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Mail\Message;
use Mail;
use App\User;
use DataTables;
use Auth;

class ItemController extends Controller 
{

  public function getByID(){
    
    $item = Item::where('user_id', \Auth::user()->id)
          ->get();
        
    return DataTables::of($item)
    ->addColumn('action', function ($item) {
                    return '
                      <a href="'.route('show-item',$item->id).'" class="btn hidden-sm-down btn-xs btn-warning"><i class="fa fa-search"></i> Show</a>
                    ';
                    })
          ->make(true);
  }

  public function getAll(){
    $item = Item::orderBy('created_at','desc')->get();
    return DataTables::of($item)
                    ->addColumn('action', function ($item) {
                      return '
                        <a href="'.route('show-item',$item->id).'" class="btn hidden-sm-down btn-xs btn-warning"><i class="fa fa-search"></i> Show</a>
                      ';
                    })
                ->make(true);
  }

  public function show_item_index(){
  	return view('item.index');
  }

  public function show_item_detail($id){
    $data = Item::where('id',$id)->first();

    return view('item.show', compact('data'));

  }

  public function add(Request $r){
    // dd($r);
    $image = request()->file('image');
    $user_id = Auth::user()->id;
    $ext = $image->extension(); 
    $imagepath = $r->name.".".$user_id.".".$ext;  
    $image->move(public_path().'/assets/img/item',$imagepath);
    $path = "assets/img/item/".$imagepath;
    $url =  \URL::to($path);


    $pet = new Item();
    $pet->name = $r->name; 
    $pet->type = $r->type; 
    $pet->price = $r->price; 
    $pet->user_id = $user_id; 
    $pet->description = $r->description; 
    $pet->photo = $url;
    $pet->save();

    return back()->with('message','Pet has been added');

  }

  public function edit_item_detail($id){
    $data = Item::where('id',$id)->first();
    // dd($data);
    return view('item.form', compact('data'));
  }

  public function update(Request $r){
    // dd($r);
    $pet = Item::where('id',$r->id)->first();
    $image = request()->file('image');
    // dd($image);
    if($image != null){
        $user_id = Auth::user()->id;
        $ext = $image->extension(); 
        $imagepath = $r->name.".".$user_id.".".$ext;  
        $image->move(public_path().'/assets/img/item',$imagepath);
        $path = "assets/img/item/".$imagepath;
        $url =  \URL::to($path);
      }else{
        $url = $pet->photo;
      }

    $pet->name = $r->name; 
    $pet->price = $r->price;  
    $pet->type = $r->type; 
    $pet->description = $r->description; 
    $pet->photo = $url;
    $pet->save();

    return redirect()->route('show-item',$pet->id);

  }

  public function delete(Request $r){

    $data = Item::where('id',$r->id)->first();
    
    $condition = Auth::user()->roles == 'admin';
    if(Auth::user()->roles == 'admin'){

      $user = User::where('id', $data->user_id)->first();
      // return $user;
      $send1 = [
                'username'  => $user->username,
                'content'   => 'Sorry. But we have to remove your ads from this platform for safety reasons',
                'ads'       => $data 
              ];

      $mail = \Mail::to($user->email)->send(new Message($send1));
    }

    $data->delete();
    return response()->json($data);
  }

  
}

?>