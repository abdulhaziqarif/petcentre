<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Shelter;

use DataTables;
use Auth;

class ShelterController extends Controller 
{

 public function getAll(){
    $shelter = Shelter::all();
    return DataTables::of($shelter)
                ->make(true);
  }

  public function show_shelter_index(){
    return view('shelter.index');
  }
  
}

?>