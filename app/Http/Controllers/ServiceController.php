<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;

use App\Mail\Message;
use Mail;
use App\User;

use DataTables;
use Auth;

class ServiceController extends Controller 
{

  public function getByID(){
    
    $service = Service::where('user_id', \Auth::user()->id)
          ->get();
        
    return DataTables::of($service)
    ->addColumn('action', function ($service) {
                    return '
                      <a href="'.route('show-service',$service->id).'" class="btn hidden-sm-down btn-xs btn-warning"><i class="fa fa-search"></i> Show</a>
                    ';
                    })
          ->make(true);
  }

  public function getAll(){
    $service = Service::orderBy('created_at','desc')->get();
    return DataTables::of($service)
                    ->addColumn('action', function ($service) {
                      return '
                        <a href="'.route('show-service',$service->id).'" class="btn hidden-sm-down btn-xs btn-danger"><i class="fa fa-search"></i> Show</a>
                      ';
                    })
                ->make(true);
  }

  public function show_service_index(){
  	return view('service.index');
  }

  public function show_service_detail($id){
    $data = Service::where('id',$id)->first();

    return view('service.show', compact('data'));

  }

  public function edit_service_detail($id){
    $data = Service::where('id',$id)->first();
    // dd($data);
    return view('service.form', compact('data'));
  }

  public function add(Request $r){
    // dd($r);
    $image = request()->file('image');
    $user_id = Auth::user()->id;
    $ext = $image->extension(); 
    $imagepath = $r->name.".".$user_id.".".$ext;  
    $image->move(public_path().'/assets/img/service',$imagepath);
    $path = "assets/img/service/".$imagepath;
    $url =  \URL::to($path);


    $pet = new Service();
    $pet->name = $r->name; 
    $pet->price = $r->price; 
    $pet->user_id = $user_id; 
    $pet->description = $r->description; 
    $pet->photo = $url;
    $pet->save();

    return back()->with('message','Pet has been added');

  }

  public function update(Request $r){
    // dd($r);
    $pet = Service::where('id',$r->id)->first();
    $image = request()->file('image');
    // dd($image);
    if($image != null){
        $user_id = Auth::user()->id;
        $ext = $image->extension(); 
        $imagepath = $r->name.".".$user_id.".".$ext;  
        $image->move(public_path().'/assets/img/service',$imagepath);
        $path = "assets/img/service/".$imagepath;
        $url =  \URL::to($path);
      }else{
        $url = $pet->photo;
      }

    $pet->name = $r->name; 
    $pet->price = $r->price;  
    $pet->description = $r->description; 
    $pet->photo = $url;
    $pet->save();

    return redirect()->route('show-service',$pet->id);

  }

  public function delete(Request $r){

    $data = Service::where('id',$r->id)->first();
     if(Auth::user()->roles == 'admin'){

      $user = User::where('id', $data->user_id)->first();
      // return $user;
      $send1 = [
                'username'  => $user->username,
                'content'   => 'Sorry. But we have to remove your ads from this platform for safety reasons',
                'ads'       => $data 
              ];

      $mail = \Mail::to($user->email)->send(new Message($send1));
    }
    $data->delete();
    return response()->json($data);
  }

  
}

?>
