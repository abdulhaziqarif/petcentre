<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use App\Models\Pet;
use App\Models\Shop;
use App\Models\Shelter;
use App\Models\Profile;

use App\Mail\Activation;

use Validator;
use Auth;

class APIShopController extends Controller
{
    public function getAllClinic(){
        header("Access-Control-Allow-Origin: *");
        $data = Shop::all();
        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$data,

        ],200);
    }

    public function getClinicDetail($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Shop::where('id',$id)
                ->first();
        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$pet,

        ],200);
    }

    public function search(Request $r){
        header("Access-Control-Allow-Origin: *");

        $pet = Shop::where('name', 'like', '%'.$r->name.'%')
                    ->where('state', $r->state)
                    ->where('town', $r->town)
                    ->get();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }
}
