<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use App\Models\Item;
use App\Models\Clinic;
use App\Models\Shelter;
use App\Models\Profile;

use App\Mail\Activation;

use Validator;
use Auth;

class APIItemController extends Controller
{
     public function getAllItem(){
        header("Access-Control-Allow-Origin: *");
        $item = Item::select('items.*','b.username')
                    ->join('users as b', 'items.user_id','=','b.id')
                    ->get();

        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$item,

        ],200);
    }

    public function getItemDetail($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Item::select('items.*','b.username')
                    ->join('users as b', 'items.user_id','=','b.id')
                    ->where('items.id',$id)
                    ->first();
        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$pet,

        ],200);
    }

     public function listItemById($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Item::select('items.*','b.username')
                    ->join('users as b', 'items.user_id','=','b.id')
                    ->where('items.user_id',$id)
                    ->get();
        return response()->json(
        [
            'message'   =>  'Success',
            'status'    =>  '200',
            'data'      =>  $pet,

        ],200);
    }

    public function addItemById(Request $r){
        header("Access-Control-Allow-Origin: *");

         $validation = Validator::make($r->all(),[
            'photo'     =>  'required',
         ]);

         if($validation->fails()){
            return response()->json(
                    [
                        'message'   =>  'Error',
                        'status'    =>  '404',
                        'data'      =>  $validation->errors(),
                    ],404);
         }

        $image      = request()->file('photo');

        if($image!=null){
            $ext        = $image->extension(); 
            $imagepath  = $r->name.$r->user_id.".".$ext;  
            $image->move(public_path().'/assets/img/pet/',$imagepath);
            $path       = "assets/img/pet/".$imagepath;
            $url        =  \URL::to($path);
        }
        else{
            $url = null;
        }
        
        $pet                = new Item();
        $pet->name          = $r->name;
        $pet->price         = $r->price;
        $pet->user_id       = $r->user_id;
        $pet->description   = $r->description;
        $pet->type          = $r->type;
        $pet->photo         = $url;

        $pet->save();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }

    public function searchitem(Request $r){
        // $pet = Pet::where('')
    }

    public function delete($id){
        header("Access-Control-Allow-Origin: *");
        $pet = Item::where('id',$id)
                ->delete();
        // $file = $pet->photo;

        // /File::delete($file);

        return response()->json(
        [
            'message'   =>  'Success',
            'status'    =>  '200',
            'data'      =>  $pet,

        ],200);
    }

    public function update(Request $r){
        header("Access-Control-Allow-Origin: *");

        $pet    = Item::where('id',$r->id)->first();

        $image  = request()->file('photo');

        if($image!=null){
            $ext        = $image->extension(); 
            $imagepath  = $r->name.$r->user_id.".".$ext;  
            $image->move(public_path().'/assets/img/pet/',$imagepath);
            $path       = "assets/img/pet/".$imagepath;
            $url        =  \URL::to($path);
        }
        else{
            $url = $pet->photo;
        }
        
        $pet->name          = $r->name;
        $pet->price         = $r->price;
        $pet->description   = $r->description;
        $pet->type          = $r->type;
        $pet->photo         = $url;          
        $pet->save();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }

    public function search(Request $r){
        header("Access-Control-Allow-Origin: *");

        $pet = Item::select('items.*','b.username')
                    ->join('users as b', 'items.user_id','=','b.id')
                    ->where('name', 'like', '%'.$r->name.'%')
                    ->where('price', '<=', $r->price)
                    ->where('type', $r->type)
                    ->get();

        return response()->json(
        [
            'message'   => 'Success',
            'status'    => '200',
            'data'      =>  $pet,

        ],200);
    }
}
