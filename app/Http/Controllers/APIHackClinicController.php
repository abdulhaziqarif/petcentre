<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\HackClinic;

class APIHackClinicController extends Controller
{
    public function getAllClinic(){
    	header("Access-Control-Allow-Origin: *");
        $data = HackClinic::all();
        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$data,

        ],200);
    }

    public function clinicdetail($id){
    	header("Access-Control-Allow-Origin: *");
        $clinic = HackClinic::where('id',$id)->first();

        $data = [
        		'clinic' 	=> $clinic,
        		'doctor' 	=> $clinic->doctors,
        		'panel'		=> $clinic->panels

        		];
        return response()->json(
        [
            'message'	=>	'Success',
            'status'	=>	'200',
            'data'		=>	$data,

        ],200);
    }

    public function search(Request $r){
        header("Access-Control-Allow-Origin: *");
        
        if($r->state == 'All' && $r->district == 'All'){
            $clinic = HackClinic::all();
        }
        else if($r->district == 'All' && $r->state !='All'){
            $clinic =   HackClinic::where('state',$r->state)
                        ->get();
        }
        else{
            $clinic =   HackClinic::where('state',$r->state)
                        ->where('district',$r->district)
                        ->get();
        }

        return response()->json(
        [
            'message'   =>  'Success',
            'status'    =>  '200',
            'data'      =>  $clinic,

        ],200);
    }
}
