<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clinic;

use DataTables;
use Auth;

class ClinicController extends Controller 
{
    public function getAll(){
      $clinic = Clinic::all();
        return DataTables::of($clinic)
              ->make(true);
  }
  
  public function show_clinic_index(){
    return view('clinic.index');
  }
  
}

?>