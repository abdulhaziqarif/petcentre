<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Shop;

use DataTables;
use Auth;

class ShopController extends Controller
{
	public function getAll(){
      $clinic = Shop::all();
        return DataTables::of($clinic)
              ->make(true);
  	}
  
	 public function show_shop_index(){
	    return view('shop.index');
	 }
}
