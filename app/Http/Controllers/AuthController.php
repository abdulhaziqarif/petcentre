<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Profile;
use App\Models\Clinic;
use App\Models\Shelter;
use App\Models\Service;
use App\Models\Shop;

use App\Mail\Activation;
use App\Mail\Message;
use Mail;

use Auth;
use Hash;
use Validator;

class AuthController extends Controller
{
    public function login(Request $r)
    {	
        
        $user = User::where('email',$r->email)->first();

        if($user!= null){
            if($user->status == 0){
                $messagefailed = "Please check your email for next step in registration";
                return redirect()->route('login')->with('messagefailed', $messagefailed);
            }
            else{
                if (Auth::attempt(['email' => $r->email, 'password' => $r->password]))
              {     
                  // Authentication passed...
                      if(Auth::user())
                      { 
                        if(Auth::user()->roles == 'admin'){
                            return redirect()->route('dashboard');
                        }
                        else{
                            return redirect()->route('clinic-index');
                        }
                      }
                      else
                      { 
                        Auth::logout();
                        $messagefailed = "Tak Jadi";
                        return redirect()->route('login')->with('messagefailed', $messagefailed);
                      }
              }
              else
              {
                  $messagefailed = " The combination email and password is not in our database";
                  return redirect()->route('login')->with('messagefailed', $messagefailed);
              }
            }
        }
        else{
                $messagefailed = " Please register if you want to use Pet Center";
                return redirect()->route('login')->with('messagefailed', $messagefailed);
        }

        // dd($user);


	    
    }

    public function register(Request $r){

        // dd($r);

    	$validation = Validator::make($r->all(),[
     		'email'=>'unique:users',
            'role'=>'required'
		 ]);

		 if($validation->fails()){
		 	 return back()->with('errors', $validation->errors());
		 }

    	$user = new User();
    	$user->email = $r->email;
    	$user->username = $r->username;
    	$user->password = bcrypt($r->password);
    	$user->photo_url = $r->photo_url;
    	$user->roles = $r->role;
        $user->status = 0;
    	$user->save();

        

    	if($r->role == 'seller'){
    	$profile = new Profile();
    	$profile->user_id = $user->id;
    	$profile->save();
    	}
    	else if($r->role == 'clinic'){
    	$profile = new Clinic();
    	$profile->user_id = $user->id;
    	$profile->save();
    	}
    	else if ($r->role == 'shelter'){
    	$profile = new Shelter();
    	$profile->user_id = $user->id;
    	$profile->save();
    	}
        else if ($r->role == 'shop'){
        $profile = new Shop();
        $profile->user_id = $user->id;
        $profile->save();
        }
        else{
            return back()->with('error','Please select role');
        }

        $link = \URL::to('activate/' . $user->id);

        $data = [
            'user_id'        => $user->id,
            'email'           => $user->email,
            'activation_link' => $link
        ];

        \Mail::to($user->email)->send(new Activation($data));
    	return redirect()->route('login')->with('warning','You have not complete yet. Please check email to use the system');
    }

    public function viewActivate($id){
        $user = \App\User::where('id',$id)->first();

        // dd($user);

        if($user->status == 1){
            return redirect()->route('login')->with('success','You have complete registration. Please login to use the system');
        }

        return view('activate',compact('user'));
    }

    public function doActivation(Request $r){
        // dd($r);
        $user = User::where('id',$r->user_id)->first();
        

        $image = request()->file('image');
        // dd($image);
        if($image != null){
            $user_id = Auth::user()->id;
            $ext = $image->extension(); 
            $imagepath = $r->name.".".$user_id.".".$ext;  
            $image->move(public_path().'/assets/img/profile',$imagepath);
            $path = "assets/img/profile/".$imagepath;
            $url =  \URL::to($path);
          }else{
            $path = "assets/img/default.png";
            $url =  \URL::to($path);
          }
        // dd($user);
        if($user->roles == 'seller'){
            $profile = Profile::where('user_id',$user->id)->first();
            $profile->address = $r->address;
            $profile->town = $r->town;
            $profile->poscode = $r->poscode;
            $profile->state = $r->state;
            $profile->country = $r->country;
            $profile->phone = $r->phone;
            $profile->photo = $url;
            $profile->save();
            
        }
        else if ($user->roles == 'shelter'){
            $shelter = Shelter::where('user_id',$user->id)->first();
            $shelter->name = $r->name;
            $shelter->address = $r->address;
            $shelter->town = $r->town;
            $shelter->poscode = $r->poscode;
            $shelter->state = $r->state;
            $shelter->country = $r->country;
            $shelter->phone = $r->phone;
            $shelter->latitude = $r->latitude;
            $shelter->longitude = $r->longitude;
            $shelter->hour_from = $r->hour_from;
            $shelter->hour_until = $r->hour_until;
            $shelter->photo = $url;
            $shelter->save();
        }
        else if($user->roles == 'clinic'){
            $clinic = Clinic::where('user_id',$user->id)->first();
            $clinic->name = $r->name;
            $clinic->address = $r->address;
            $clinic->town = $r->town;
            $clinic->poscode = $r->poscode;
            $clinic->state = $r->state;
            $clinic->country = $r->country;
            $clinic->phone = $r->phone;
            $clinic->latitude = $r->latitude;
            $clinic->longitude = $r->longitude;
            $clinic->hour_from = $r->hour_from;
            $clinic->hour_until = $r->hour_until;
            $clinic->photo = $url;
            $clinic->save();
        }
        else{
            $clinic = Shop::where('user_id',$user->id)->first();
            $clinic->name = $r->name;
            $clinic->address = $r->address;
            $clinic->town = $r->town;
            $clinic->poscode = $r->poscode;
            $clinic->state = $r->state;
            $clinic->country = $r->country;
            $clinic->phone = $r->phone;
            $clinic->latitude = $r->latitude;
            $clinic->longitude = $r->longitude;
            $clinic->hour_from = $r->hour_from;
            $clinic->hour_until = $r->hour_until;
            $clinic->photo = $url;
            $clinic->save();
        }

        $user->status = 1;
        $user->photo_url = $url;
        $user->save();

        return redirect()->route('login')->with('success','You have complete registration. Please login to use the system');
    }
}
