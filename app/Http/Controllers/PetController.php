<?php 

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Models\Pet;
use App\User;

use App\Mail\Message;
use Mail;

use DataTables;
use Auth;
use File;


class PetController extends Controller 
{

  public function getByID(){
    $pet = Pet::where('user_id', Auth::user()->id)->get();
    return DataTables::of($pet)
                  ->addColumn('action', function ($pet) {
                    return '
                      <a href="'.route('show-pet',$pet->id).'" class="btn hidden-sm-down btn-xs btn-warning"><i class="fa fa-search"></i> Show</a>
                    ';
                    })
                ->make(true);
  }

  public function getAll(){
    $pet = Pet::orderBy('created_at','desc')->get();
    return DataTables::of($pet)
                 ->addColumn('action', function ($pet) {
                    return '
                      <a href="'.route('show-pet',$pet->id).'" class="btn hidden-sm-down btn-xs btn-warning"><i class="fa fa-search"></i> Show</a>
                    ';
                    })
                ->make(true);
  }

  public function show_pet_index(){
    // $condition = Auth::user()->roles == 'admin';
    // dd($condition);
  	return view('pet.index');
  }

  public function add(Request $r){
    // dd($r);
    $image = request()->file('image');
    $user_id = Auth::user()->id;
    $ext = $image->extension(); 
    $imagepath = $r->name.".".$user_id.".".$ext;  
    $image->move(public_path().'/assets/img/pet',$imagepath);
    $path = "assets/img/pet/".$imagepath;
    $url =  \URL::to($path);


    $pet = new Pet();
    $pet->name = $r->name; 
    $pet->year = $r->year; 
    $pet->month = $r->month; 
    $pet->price = $r->price; 
    $pet->type = $r->type;  
    $pet->user_id = $user_id; 
    $pet->description = $r->description; 
    $pet->photo = $url;
    $pet->save();

    return back()->with('message','Pet has been added');

  }

  public function show_pet_detail($id){
    $data = Pet::where('id',$id)->first();
    // dd($data);
    return view('pet.show', compact('data'));
  }

  public function delete(Request $r){

    $data = Pet::where('id',$r->id)->first();
    $condition = Auth::user()->roles == 'admin';
    if(Auth::user()->roles == 'admin'){

      $user = User::where('id', $data->user_id)->first();
      // return $user;
      $send1 = [
                'username'  => $user->username,
                'content'   => 'Sorry. But we have to remove your ads from this platform for safety reasons',
                'ads'       => $data 
              ];

      $mail = \Mail::to($user->email)->send(new Message($send1));
    }
    $data->delete();
    return response()->json($data);
  }

  public function delete_by_user($id){

    $data = Pet::where('id',$id)->first();

    if(\Auth::user()->roles == 'admin'){

      $user = User::where('id', $data->user_id)->first();
      $data = [
                'username'  => $user->username,
                'content'   => 'Sorry. But we have to remove your ads from this platform for safety reasons',
                'ads'       => $data 
              ];

      \Mail::to($user->email)->send(new Message($data));  
    }
    

    $data->delete();


    return redirect()->route('pet-index');
  }

  public function edit_pet_detail($id){
    $data = Pet::where('id',$id)->first();
    // dd($data);
    return view('pet.form', compact('data'));
  }

  public function update(Request $r){
    // dd($r);
    $pet = Pet::where('id',$r->id)->first();
    $image = request()->file('image');
    // dd($image);
    if($image != null){
        $user_id = Auth::user()->id;
        $ext = $image->extension(); 
        $imagepath = $r->name.".".$user_id.".".$ext;  
        $image->move(public_path().'/assets/img/pet',$imagepath);
        $path = "assets/img/pet/".$imagepath;
        $url =  \URL::to($path);
      }else{
        $url = $pet->photo;
      }

    $pet->name = $r->name; 
    $pet->year = $r->year; 
    $pet->month = $r->month; 
    $pet->price = $r->price;  
    $pet->type = $r->type;  
    $pet->description = $r->description; 
    $pet->photo = $url;
    $pet->save();

    return redirect()->route('show-pet',$pet->id);

  }

  
}

?>