<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Clinic;
use App\Models\Shop;
use App\Models\Shelter;
use App\Models\Profile;
use App\Models\Pet;
use App\Models\Item;
use App\Models\Service;

use App\Mail\Activation;
use App\Mail\Message;
use Mail;

use App\User;
use Auth;

use DataTables;

class UserController extends Controller 
{

  public function getAll(){
    $user = User::all();
    return DataTables::of($user)
                ->make(true);
  }

  public function show_user_index(){
    return view('user.index');
  }

  public function verify($id){
  	$user = User::where('id',$id)->first();
  	$user->review = 1;
  	$user->save();

  	return redirect()->route('dashboard');
  }

  public function delete($id){
  	$user = User::where('id',$id)->first();
  	$pet = Pet::where('user_id',$id)->delete();
  	$item = Item::where('user_id',$id)->delete();
  	$service = Service::where('user_id',$id)->delete();

  	$send1 = [
                'username'  => $user->username,
                'content'   => 'Sorry. But we have to remove your account for safety reasons',
                'ads'       => $data 
              ];

    $mail = \Mail::to($user->email)->send(new Message($send1));

    $user->delete();

  	return redirect()->route('dashboard');

  }
  
}

?>