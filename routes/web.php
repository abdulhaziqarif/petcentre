<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// LOGOUT
Route::get('logout', function(){
    \Auth::logout();
    // dd('masuk');
    return redirect()->route('login');
  })->name('logout');

Route::get('test', function(){
    return view('pet.form');
});

Route::get('/', function(){
    return view('login');
});

Route::get('test2', function(){
    return view('dashboard');
});

Route::get('activate/{id}',[
    'as'=>'activate',
    'uses' => 'AuthController@viewActivate'
]);

Route::post('activate',[
    'as'=>'activate',
    'uses' => 'AuthController@doActivation'
]);

Route::group(['middleware'=>'guest'], function()
{
    Route::get('login',['as' => 'login','uses' => 'ViewController@show_login']);
    Route::get('register',['as' => 'register','uses' => 'ViewController@show_register']);
    Route::post('login',['as' => 'login','uses' => 'AuthController@login']);
    Route::post('register',['as' => 'register','uses' => 'AuthController@register']);
});


Route::group(['middleware'=>'auth'],function()
{
    
    //////////////////////////////////////////////  DATATABLES  
    Route::group(['prefix'=>'datatable'], function(){
        //////////////////////////////////////////////  USER CONTROLLER
        // Get All User
        Route::get('user-list',['as' => 'ajax-user-list','uses'=> 'UserController@getAll']);
        //////////////////////////////////////////////  PET CONTROLLER
        // Pet By ID
        Route::get('pet-list-id',['as' => 'ajax-pet-list-id','uses'=> 'PetController@getByID']);
        // Get All Pet
        Route::get('pet-list',['as' => 'ajax-pet-list','uses'=> 'PetController@getAll']);
        //////////////////////////////////////////////  ITEM CONTROLLER
        // Item By ID
        Route::get('item-list-id',['as' => 'ajax-item-list-id','uses'=> 'ItemController@getByID']);
        // Get All Item
        Route::get('item-list',['as' => 'ajax-item-list','uses'=> 'ItemController@getAll']);
        //////////////////////////////////////////////  ITEM CONTROLLER
        // Service By ID
        Route::get('service-list-id',['as' => 'ajax-service-list-id','uses'=> 'ServiceController@getByID']);
        // Get All Service
        Route::get('service-list',['as' => 'ajax-service-list','uses'=> 'ServiceController@getAll']);
        //////////////////////////////////////////////  CLINIC CONTROLLER
        // Get All Clinic
        Route::get('clinic-list',['as' => 'ajax-clinic-list','uses'=> 'ClinicController@getAll']);
        //////////////////////////////////////////////  SHELTER CONTROLLER
        // Get All Shelter
        Route::get('shelter-list',['as' => 'ajax-shelter-list','uses'=> 'ShelterController@getAll']);
        //////////////////////////////////////////////  SHOP CONTROLLER
        // Get All Shop
        Route::get('shop-list',['as' => 'ajax-shop-list','uses'=> 'ShopController@getAll']);


    });

    //////////////////////////////////////////////  VIEW CONTROLLER
    Route::get('index',['as' => 'index','uses' => 'ViewController@show_index']);
    Route::get('dashboard',['as' => 'dashboard','uses' => 'ViewController@show_dashboard']);
    Route::get('profile/{id}',['as' => 'profile','uses' => 'ViewController@show_profile']);
    Route::post('updateprofile',['as' => 'updateprofile','uses' => 'ViewController@updateprofile']);
    //////////////////////////////////////////////  USER CONTROLLER
    Route::get('user-index',['as' => 'user-index','uses' => 'UserController@show_user_index']);
    Route::get('verify/{id}',['as' => 'verify','uses' => 'UserController@verify']);
    Route::get('delete-account/{id}',['as' => 'delete-account','uses' => 'UserController@delete']);
    //////////////////////////////////////////////  PET CONTROLLER
    Route::get('pet-index',['as' => 'pet-index','uses' => 'PetController@show_pet_index']);
    Route::post('add-pet',['as' => 'add-pet','uses' => 'PetController@add']);
    Route::get('show-pet/{id}',['as' => 'show-pet','uses' => 'PetController@show_pet_detail']);
    Route::get('edit-pet/{id}',['as' => 'edit-pet','uses' => 'PetController@edit_pet_detail']);
    Route::get('delete-pet-by-user/{id}',['as' => 'delete-pet-by-user','uses' => 'PetController@delete_by_user']);
    Route::post('delete-pet',['as' => 'delete-pet','uses' => 'PetController@delete']);
    Route::post('update-pet',['as' => 'update-pet','uses' => 'PetController@update']);
    //////////////////////////////////////////////  ITEM CONTROLLER
    Route::get('item-index',['as' => 'item-index','uses' => 'ItemController@show_item_index']);
    Route::post('add-item',['as' => 'add-item','uses' => 'ItemController@add']);
    Route::get('show-item/{id}',['as' => 'show-item','uses' => 'ItemController@show_item_detail']);
    Route::get('edit-item/{id}',['as' => 'edit-item','uses' => 'ItemController@edit_item_detail']);
    Route::post('delete-item',['as' => 'delete-item','uses' => 'ItemController@delete']);
    Route::post('update-item',['as' => 'update-item','uses' => 'ItemController@update']);
    //////////////////////////////////////////////  SERVICE CONTROLLER
    Route::get('service-index',['as' => 'service-index','uses' => 'ServiceController@show_service_index']);
    Route::post('add-service',['as' => 'add-service','uses' => 'ServiceController@add']);
    Route::get('show-service/{id}',['as' => 'show-service','uses' => 'ServiceController@show_service_detail']);
    Route::get('edit-service/{id}',['as' => 'edit-service','uses' => 'ServiceController@edit_service_detail']);
    Route::post('delete-service',['as' => 'delete-service','uses' => 'ServiceController@delete']);
    Route::post('update-service',['as' => 'update-service','uses' => 'ServiceController@update']);
    //////////////////////////////////////////////  CLINIC CONTROLLER
    Route::get('clinic-index',['as' => 'clinic-index','uses' => 'ClinicController@show_clinic_index']);
    //////////////////////////////////////////////  SHELTER CONTROLLER
    Route::get('shelter-index',['as' => 'shelter-index','uses' => 'ShelterController@show_shelter_index']);
    //////////////////////////////////////////////  SHELTER CONTROLLER
    Route::get('shop-index',['as' => 'shop-index','uses' => 'ShopController@show_shop_index']);
    
 

});