<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// APIAuthControlller
Route::post('api-register', 'APIAuthController@register');
Route::post('api-login', 'APIAuthController@login');

// APIPetController
Route::get('pet-list', 'APIPetController@getAllPet');
Route::get('pet-detail/{id}', 'APIPetController@getPetDetail');
Route::get('pet-list/individual/{id}', 'APIPetController@listPetById');
Route::get('pet-delete/individual/{id}', 'APIPetController@delete');
Route::post('pet/addPetById', 'APIPetController@addPetById');
Route::post('pet/update', 'APIPetController@update');
Route::post('pet/search', 'APIPetController@search');

// APIItemController
Route::get('item-list', 'APIItemController@getAllItem');
Route::get('item-detail/{id}', 'APIItemController@getItemDetail');
Route::get('item-list/individual/{id}', 'APIItemController@listItemById');
Route::get('item-delete/individual/{id}', 'APIItemController@delete');
Route::post('item/addItemById', 'APIItemController@addItemById');
Route::post('item/update', 'APIItemController@update');
Route::post('item/search', 'APIItemController@search');

// APIServiceController
Route::get('service-list', 'APIServiceController@getAllItem');
Route::get('service-detail/{id}', 'APIServiceController@getItemDetail');
Route::get('service-list/individual/{id}', 'APIServiceController@listItemById');
Route::get('service-delete/individual/{id}', 'APIServiceController@delete');
Route::post('service/addItemById', 'APIServiceController@addItemById');
Route::post('service/update', 'APIServiceController@update');
Route::post('service/search', 'APIServiceController@search');

// APIClinicController
Route::get('clinic-list', 'APIClinicController@getAllClinic');
Route::get('clinic-detail/{id}', 'APIClinicController@getClinicDetail');
Route::post('clinic/search', 'APIClinicController@search');

// APIShopController
Route::get('shop-list', 'APIShopController@getAllClinic');
Route::get('shop-detail/{id}', 'APIShopController@getClinicDetail');
Route::post('shop/search', 'APIShopController@search');

// APIShelterControlller
Route::get('shelter-list', 'APIShelterController@getAllShelter');
Route::get('shelter-detail/{id}', 'APIShelterController@getShelterDetail');
Route::post('shelter/search', 'APIShelterController@search');

// HackathonController
Route::get('hack/clinic/all', 'APIHackClinicController@getAllClinic');
Route::get('hack/clinic/detail/{id}', 'APIHackClinicController@clinicdetail');
Route::post('hack/clinic/search', 'APIHackClinicController@search');



