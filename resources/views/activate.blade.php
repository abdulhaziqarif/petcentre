<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Monster Admin Template - The Most Complete & Trusted Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="../css/colors/blue.css" id="theme" rel="stylesheet">
    <link href="../assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="../assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />

    <link href="../assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/plugins/dropify/dist/css/dropify.min.css">
    
    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    
        <div class="login-register" style="background-image:url(../assets/images/background/login-register.jpg);overflow-y: auto;">        
            <div class="login-box card">
            <form class="form-horizontal form-material" id="loginform" method="post" action="{{route('activate')}}">
            <div class="card-block">
                @if(session()->has('errors'))
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                             <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @if ($message = Session::get('error'))
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <h3 class="text-danger"><i class="fa fa-exclamation-triangle"></i> Error</h3>
                            {!! e(Session::get('error')) !!}
                        </div>
                    @endif
                    @csrf
                    <h3 class="box-title m-b-20">Activation Account</h3>
                    <div class="form-group ">
                    @if($user->roles == 'seller')
                    @else
                    <div class="col-xs-12">
                            <!-- <label>Name</label> -->
                            <input class="form-control" type="text" placeholder="Name" name="name" required="">
                        </div>
                    @endif
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input class="form-control" type="text" placeholder="Phone" name="phone" required>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Address" name="address" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="Poscode" name="poscode" required="">
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="Town" name="town" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="State" name="state" required="">
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="Country" name="country" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label>Profile Photo</label>
                            <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100"  name="image" />
                        </div>
                    </div>
                    @if($user->roles == 'seller')
                    
                    @else
                    
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Open From: </label>
                            <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" value="24 hours format" name="hour_from"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Open Until: </label>
                            <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                <input type="text" class="form-control" value="24 hours format" name="hour_until"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <!-- <input class="form-control" type="text" placeholder="title" name="title" required=""> -->
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="Search Location" id="searchmap">
                        </div>
                    </div>
                    <div class="row">
                        <div id="map"></div>

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="latitude" name="latitude" required="" id="lat">
                        </div>
                        <div class="col-md-6">
                            <input class="form-control" type="text" placeholder="longitude" name="longitude" required="" id="lng">
                        </div>
                    </div>
                    @endif
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-success btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Activate</button>
                    </div>
                </div>
                </form>
                
            </div>
          </div>
    
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="../js/custom.min.js"></script>
    <script src="../assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="../assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <script src="../assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
     <script>
      function initMap() {
       var myLatlng = new google.maps.LatLng(1.558144536481747,103.6378740877442);
        var mapOptions = {
          zoom: 15,
          center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        // Place a draggable marker on the map
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Drag me!"
        });

        var searhBox = new google.maps.places.SearchBox(document.getElementById("searchmap"));

        google.maps.event.addListener(searhBox,'places_changed',function(){
            var places = searhBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0; place = places[i];i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(15);

            
        });

        google.maps.event.addListener(marker,'position_changed', function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        })

      }

      $('#loginform').on('keyup keypress', function(e) {
          var keyCode = e.keyCode || e.which;
          if (keyCode === 13) { 
            e.preventDefault();
            return false;
          }
        });

      // For select 2
        $(".select2").select2();
        $('.selectpicker').selectpicker();

        // Clock Picker
         $('.clockpicker').clockpicker({
            donetext: 'Done',
        }).find('input').change(function() {
            console.log(this.value);
        });

        $('.dropify').dropify();
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGqNCdKrcm0uCfmV02t_qSC4KA4LHh4Y4&callback=initMap&libraries=places">
    </script>

</body>

</html>