@extends('master')

@section('body')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">

                <form>
            	<div class="row">
            		<div class="col-md-12">
                        <a class="btn btn-sm btn-white btn-warning btn-round pull-right" style="margin-left:2px;" href="javascript:window.history.back();">
                            <i class="fa fa-undo"></i> Back
                        </a>
                        <a class="btn btn-sm btn-white btn-danger btn-round pull-right" style="margin-left:2px;" href="#" id="sa-params">
                            <i class="fa fa-trash"></i> Delete
                        </a>
                        @if(Auth::user()->id == $data->user_id)
                        <a class="btn btn-sm btn-white btn-primary btn-round pull-right" style="margin-left:2px;" href="{{route('edit-pet',$data->id)}}">
                            <i class="fa fa-pencil"></i> Edit
                        </a>
                        @endif
            		</div>
            	</div>
            	<br>
                <div class="row">
                	<div class="col-md-12">
						<div class="form-group row">
						  <label for="example-text-input" class="col-2 col-form-label">Name</label>
						  <div class="col-10">
						    <input class="form-control" type="text" value="{{$data->name}}" id="example-text-input" readonly="">
                            <input class="form-control" type="hidden" value="{{$data->id}}" id="data_id" name="id">
                            <input class="form-control" type="hidden" value="{{csrf_token()}}" id="token" name="token">
						  </div>
						</div>
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Year</label>
                          <div class="col-10">
                            <input class="form-control" type="number" value="{{$data->year}}" id="example-text-input" readonly="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Month</label>
                          <div class="col-10">
                            <input class="form-control" type="number" value="{{$data->month}}" id="example-text-input" readonly="">
                          </div>
                        </div>
						<div class="form-group row">
						  <label for="example-text-input" class="col-2 col-form-label">Price (RM)</label>
						  <div class="col-10">
						    <input class="form-control" type="number" value="{{$data->price}}" id="example-text-input" readonly="">
						  </div>
						</div>
						<div class="form-group row">
						  <label for="example-text-input" class="col-2 col-form-label">Description</label>
						  <div class="col-10">
                            <textarea class="form-control" id="message-text" name="description" readonly="">{{$data->description}}</textarea>	
						  </div>
						</div>
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Type</label>
                          <div class="col-10">
                            <select class="custom-select col-12" id="inlineFormCustomSelect" name="type" required="" disabled="">
                                <option selected="" disabled="">Type...</option>
                                <option value="Mammal" <?php echo ($data->type == "Mammal")?"selected":""; ?> >Mammal</option>
                                <option value="Bird" <?php echo ($data->type == "Bird")?"selected":""; ?> >Bird</option>
                                <option value="Fish" <?php echo ($data->type == "Fish")?"selected":""; ?> >Fish</option>
                                <option value="Reptile" <?php echo ($data->type == "Reptile")?"selected":""; ?> >Reptile</option>
                                <option value="Amphambian" <?php echo ($data->type == "Amphambian")?"selected":""; ?> >Amphambian</option>
                                <option value="Invertebrate" <?php echo ($data->type == "Invertebrate")?"selected":""; ?> >Invertebrate</option>
                            </select>
                          </div>
                        </div>
						<div class="form-group row">
						  <label for="example-text-input" class="col-2 col-form-label">Picture</label>
						  <div class="col-10">
                             <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100" data-show-remove="false" readonly="" data-default-file="{{$data->photo}}" />
						  </div>
						</div>
						</div>
                	</div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('javascript')
 <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });

        $('#sa-params').click(function(){
        var id2 = $('#data_id').val();
        var token = $('#token').val();
        console.log(id2);
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false,
        }, function(isConfirm){   
            if (isConfirm) {     
                $.ajax({
                    url: "<?php echo route('delete-pet') ?>",
                    type: "POST",
                    data:{ id:id2, _token:token},
                    success: function (data) {
                        // swal("Error deleting!", "Please try again", "error");
                        // console.log(data);
                        swal({
                                title: "Done!",
                                text: "It was succesfully deleted!",
                                type: "success",
                                showCancelButton: false
                            },function(){
                                window.location.href="{!! route('pet-index') !!}";
                            });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error deleting!", "Please try again", "error");
                    }
                });   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
                } 
            });
        });
    });
    </script>
@endpush