@extends('master')

@section('body')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                	<div class="col-md-12">
                		<button class="btn pull-right hidden-sm-down btn-success" data-toggle="modal" data-target="#responsive-modal"><i class="mdi mdi-plus-circle"></i> Add Pet</button>
                	</div>
                </div>
                <div class="row">
                    <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Add Pet</h4>
                                </div>
                                <form method="post" action="{{route('add-pet')}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                        @csrf
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Name:</label>
                                            <input type="text" class="form-control" id="recipient-name" name="name" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Year:</label>
                                            <input type="number" class="form-control" id="recipient-name" name="year" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Month:</label>
                                            <input type="number" class="form-control" id="recipient-name" name="month" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Price (RM):</label>
                                            <input type="number" class="form-control" id="recipient-name" name="price" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Description:</label>
                                            <textarea class="form-control" id="message-text" name="description" required=""></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Role</label>
                                             <select class="custom-select col-12" id="inlineFormCustomSelect" name="type" required="">
                                                <option selected="" disabled="">Type...</option>
                                                <option value="Mammal">Mammal</option>
                                                <option value="Bird">Bird</option>
                                                <option value="Fish">Fish</option>
                                                <option value="Reptile">Reptile</option>
                                                <option value="Amphambian">Amphambian</option>
                                                <option value="Invertebrate">Invertebrate</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="input-file-now-custom-2">Image</label>
                                            <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100" required="" name="image" />
                                        </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light">Add Pet</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.modal -->
                </div>
                <br>
                <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Price (RM)</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
                </thead>
              </table>


            </div>
        </div>
    </div>
</div>
@stop

@push('css')

@endpush

@push('javascript')
@if(Auth::user()->roles == 'admin')
<script>
$(function() {
    $('#example2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajax-pet-list') !!}',
        columns: [
            { data: 'photo', name: 'photo', render: function (data, type, full, meta) { return "<img src=\"" + data + "\" height=\"50\"/>";} },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'description', name: 'description' },
            {data: 'action', name: 'action', orderable: false, searchable: false}            
        ],
    });
});
</script>
@else
<script>
$(function() {
    $('#example2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajax-pet-list-id') !!}',
        columns: [
            // { data: 'image', name: 'image' },
            { data: 'photo', name: 'photo', render: function (data, type, full, meta) { return "<img src=\"" + data + "\" height=\"50\"/>";} },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'description', name: 'description' },
            {data: 'action', name: 'action', orderable: false, searchable: false}            
        ],
    });
});
</script>
@endif
 <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
@endpush