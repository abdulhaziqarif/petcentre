@extends('master')

@section('body')
<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-xlg-3 col-md-5">
        <div class="card">
            <div class="card-body">
                <center class="m-t-30"> 
                    @if($user->profile_id != null)
                    <img src="{{$user->photo_url}}" class="img-circle" width="150" />
                    @else
                    <img src="{{$user->photo}}" class="img-circle" width="150" />
                    @endif
                    <!-- <img src="../assets/images/users/5.jpg" class="img-circle" width="150" /> -->
                    <h4 class="card-title m-t-10">
                            {{$user->username}}
                    </h4>
                    <h6 class="card-subtitle">
                    @if($user->profile_id != null)
                    @else
                    {{$user->name}}
                    @endif
                    </h6>
                </center>
            </div>
            <div>
                <hr> </div>
            <div class="card-body" style="padding-left: 10px;"> 
                <small class="text-muted">Email address </small>
                <h6>{{$user->email}}</h6> 
                <small class="text-muted p-t-30 db">Phone</small>
                <h6>{{$user->phone}}</h6> 
                <small class="text-muted p-t-30 db">Address</small>
                <h6>{{$user->address}}</h6>
                <div class="map-box" id="map">
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-8 col-xlg-9 col-md-7">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs profile-tab" role="tablist">
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Profile</a> </li>
                 @if(Auth::user()->id == $user->user_id)
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Settings</a> </li>
                @endif
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!--second tab-->
                <div class="tab-pane active" id="profile" role="tabpanel">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                                <br>
                                <p class="text-muted">
                                        {{$user->username}}
                                </p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                <br>
                                <p class="text-muted">{{$user->phone}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                <br>
                                <p class="text-muted">{{$user->email}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                                <br>
                                <p class="text-muted">{{$user->state}}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                        @if(Auth::user()->roles == 'admin')
                            <div class="col-md-12">
                                @if($user->review == 0)
                                <a class="btn btn-sm btn-white btn-success btn-round" style="margin: 10px;" href="{{route('verify',$user->user_id)}}">
                                    <i class="fa fa-check"></i> Verify Account
                                </a>
                                @endif
                                <a class="btn btn-sm btn-white btn-danger btn-round" style="margin: 10px;" href="{{route('delete-account',$user->user_id)}}">
                                    <i class="fa fa-trash"></i> Delete Account
                                </a>
                            </div>
                        @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="settings" role="tabpanel">
                    <div class="card-body">
                        <form class="form-horizontal form-material" enctype="multipart/form-data" method="post" action="{{route('updateprofile')}}">
                            <div class="form-group">
                                <label class="col-md-12">Full Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" value="{{$user->username}}" name="username">
                                </div>
                            </div>
                            @csrf
                            <input type="hidden" class="form-control form-control-line" name="id" value="{{$user->user_id}}">
                            <input type="hidden" class="form-control form-control-line" name="clinic_id" value="{{$user->clinic_id}}">
                            <input type="hidden" class="form-control form-control-line" name="shelter_id" value="{{$user->shelter_id}}">
                            <input type="hidden" class="form-control form-control-line" name="shop_id" value="{{$user->shop_id}}">
                            @if($user->profile_id != null)                                    
                            <input type="hidden" class="form-control form-control-line" name="photo_url" value="{{$user->photo_url}}">
                            @else
                            <div class="form-group">
                                <label class="col-md-12">Company Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="company_name" value="{{$user->name}}">

                                    <input type="hidden" class="form-control form-control-line" name="photo_url" value="{{$user->photo}}">
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Email</label>
                                <div class="col-md-12">
                                    <input type="email" class="form-control form-control-line" name="email" id="example-email" value="{{$user->email}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Phone No</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="phone" value="{{$user->phone}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Address</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="address" value="{{$user->address}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Poscode</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="poscode" value="{{$user->poscode}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">District</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="town" value="{{$user->town}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">State</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="state" value="{{$user->state}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">State</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="country" value="{{$user->country}}">
                                </div>
                            </div>
                            @if($user->profile_id != null)
                            @else
                            <div class="form-group">
                                <label class="col-md-12">Hour From</label>
                                <div class="col-md-12">
                                    <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                        <input type="text" class="form-control" name="hour_from" value="{{$user->hour_from}}"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Hour Until</label>
                                <div class="col-md-12">
                                   <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                        <input type="text" class="form-control" name="hour_until" value="{{$user->hour_until}}"> <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Search Location</label>
                                <div class="col-md-12">
                                   <input class="form-control" type="text" placeholder="Search Location" id="searchmap">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Latitude</label>
                                <div class="col-md-12">
                                   <input class="form-control" type="text" placeholder="latitude" name="latitude" value="{{$user->latitude}}" id="lat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Longitude</label>
                                <div class="col-md-12">
                                   <input class="form-control" type="text" placeholder="longitude" name="longitude" value="{{$user->longitude}}" required="" id="lng">
                                </div>
                            </div>
                            @endif
                            @if($user->profile_id != null)
                            <div class="form-group">
                                <label class="col-md-12">Photo</label>
                                <div class="col-md-12">
                                   <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100" data-default-file="{{$user->photo_url}}" name="image" />
                                </div>
                            </div>
                            @else
                            <div class="form-group">
                                <label class="col-md-12">Photo</label>
                                <div class="col-md-12">
                                   <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100" data-default-file="{{$user->photo}}" name="image" />
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success" type="submit">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
@stop

@push('javascript')
    <link href="../assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>
    <script src="../assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>

<script type="text/javascript">

     function initMap() {
        var lat = $('#lat').val();
        var long = $('#lng').val();
       var myLatlng = new google.maps.LatLng(lat,long);
        var mapOptions = {
          zoom: 15,
          center: myLatlng
        }
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);

        // Place a draggable marker on the map
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            draggable:true,
            title:"Drag me!"
        });

        var searhBox = new google.maps.places.SearchBox(document.getElementById("searchmap"));

        google.maps.event.addListener(searhBox,'places_changed',function(){
            var places = searhBox.getPlaces();
            var bounds = new google.maps.LatLngBounds();
            var i, place;

            for(i=0; place = places[i];i++){
                bounds.extend(place.geometry.location);
                marker.setPosition(place.geometry.location);
            }

            map.fitBounds(bounds);
            map.setZoom(15);

            
        });

        google.maps.event.addListener(marker,'position_changed', function(){
            var lat = marker.getPosition().lat();
            var lng = marker.getPosition().lng();

            $('#lat').val(lat);
            $('#lng').val(lng);
        })

      }

      $('#loginform').on('keyup keypress', function(e) {
          var keyCode = e.keyCode || e.which;
          if (keyCode === 13) { 
            e.preventDefault();
            return false;
          }
        });

     // Clock Picker
     $('.clockpicker').clockpicker({
        donetext: 'Done',
    }).find('input').change(function() {
        console.log(this.value);
    });

    $('.dropify').dropify();
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGqNCdKrcm0uCfmV02t_qSC4KA4LHh4Y4&callback=initMap&libraries=places">
</script>
@endpush