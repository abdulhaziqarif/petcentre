@extends('master')

@section('body')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                	<div class="col-md-12">
                        <button class="btn pull-right hidden-sm-down btn-success" data-toggle="modal" data-target="#responsive-modal"><i class="mdi mdi-plus-circle"></i> Add Item</button>
                	</div>
                </div>
                <div class="row">
                    <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title">Add Item</h4>
                                </div>
                                <form method="post" action="{{route('add-item')}}" enctype="multipart/form-data">
                                <div class="modal-body">
                                        @csrf
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Name:</label>
                                            <input type="text" class="form-control" id="recipient-name" name="name" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Type:</label>
                                            <select class="custom-select col-12" id="inlineFormCustomSelect" name="type" required="">
                                                <option selected="" disabled="">As...</option>
                                                <option value="New">New</option>
                                                <option value="Second">Second</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="control-label">Price (RM):</label>
                                            <input type="number" class="form-control" id="recipient-name" name="price" required="">
                                        </div>
                                        <div class="form-group">
                                            <label for="message-text" class="control-label">Description:</label>
                                            <textarea class="form-control" id="message-text" name="description" required=""></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="input-file-now-custom-2">Image</label>
                                            <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100" required="" name="image" required="" />
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger waves-effect waves-light">Add</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.modal -->
                </div>
                <br>
                <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Price (RM)</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>Action</th>
                </tr>
                </thead>
              </table>


            </div>
        </div>
    </div>
</div>
@stop

@push('javascript')
@if(Auth::user()->roles == 'admin')
<script>
$(function() {
    $('#example2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajax-item-list') !!}',
        columns: [
            { data: 'photo', name: 'photo', render: function (data, type, full, meta) { return "<img src=\"" + data + "\" height=\"50\"/>";} },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'type', name: 'type' },
            { data: 'description', name: 'description' },
            {data: 'action', name: 'action', orderable: false, searchable: false}            
        ],
    });
});
</script>
@else
<script>
$('.dropify').dropify();
$(function() {
    $('#example2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajax-item-list-id') !!}',
        columns: [
            { data: 'photo', name: 'photo', render: function (data, type, full, meta) { return "<img src=\"" + data + "\" height=\"50\"/>";} },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'type', name: 'type' },
            { data: 'description', name: 'description' },            
            {data: 'action', name: 'action', orderable: false, searchable: false}            
        ],
    });
});
</script>
@endif
@endpush