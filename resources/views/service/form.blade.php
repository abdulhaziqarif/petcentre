@extends('master')

@section('body')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">

                <form method="post" action="{{ route('update-service') }}" enctype="multipart/form-data">
            	<div class="row">
            		<div class="col-md-12">
                        <a class="btn btn-sm btn-white btn-warning btn-round pull-right" style="margin-left:2px;" href="javascript:window.history.back();">
                            <i class="fa fa-undo"></i> Cancel
                        </a>
                        @csrf
                        <button class="btn btn-sm btn-white btn-success btn-round pull-right" type="submit" style="margin-left:2px">
                            <i class="fa fa-floppy-o"></i> Save
                        </button>
            		</div>
            	</div>
            	<br>
                <div class="row">
                	<div class="col-md-12">
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Name</label>
                          <div class="col-10">
                            <input class="form-control" type="text" value="{{$data->name}}" id="example-text-input" name="name" >
                            <input class="form-control" type="hidden" value="{{$data->id}}" id="data_id" name="id">
                            <input class="form-control" type="hidden" value="{{csrf_token()}}" id="token" name="token">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Price (RM)</label>
                          <div class="col-10">
                            <input class="form-control" type="number" value="{{$data->price}}" id="example-text-input" name="price">
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Description</label>
                          <div class="col-10">
                            <textarea class="form-control" id="message-text" name="description" >{{$data->description}}</textarea>   
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="example-text-input" class="col-2 col-form-label">Picture</label>
                          <div class="col-10">
                                <input type="file" id="input-file-now-custom-2" class="dropify" data-height="100" data-default-file="{{$data->photo}}" name="image" />
                          </div>
                        </div>
                        </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('javascript')
 <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
@endpush