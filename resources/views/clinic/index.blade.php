@extends('master')

@section('body')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <div class="row">
                	<div class="col-md-12">
                		<!-- <button class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> Add Pet</button> -->
                	</div>
                </div>
                <br>
                <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Address</th>
                  <th>Phone</th>
                  <th>Open From</th>
                  <th>Open until</th>
                </tr>
                </thead>
              </table>


            </div>
        </div>
    </div>
</div>
@stop

@push('javascript')
<script>
$(function() {
    $('#example2').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajax-clinic-list') !!}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'address', name: 'address' },
            { data: 'phone', name: 'phone' }, 
            { data: 'hour_from', name: 'hour_from' },            
            { data: 'hour_until', name: 'hour_until' },            
        ],
    });
});
</script>
@endpush