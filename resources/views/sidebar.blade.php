@if (Auth::user()->roles == 'admin')
<li class="nav-devider"></li>
<li class="nav-small-cap">DASHBOARD</li>
<li>
    <a  href="{{route('dashboard')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
</li>
<li class="nav-devider"></li>
<li class="nav-small-cap">USER</li>
<li>
    <a class="" href="{{route('user-index')}}" aria-expanded="false"><i class="fa fa-user"></i><span class="hide-menu">User</span></a>
</li>
<li>
    <a class="" href="{{route('shop-index')}}" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span class="hide-menu">Shop</span></a>
</li>
<li>
    <a class="" href="{{route('clinic-index')}}" aria-expanded="false"><i class="fa fa-plus-square"></i><span class="hide-menu">Clinic</span></a>
</li>
<li>
    <a class="" href="{{route('shelter-index')}}" aria-expanded="false"><i class="fa fa-home"></i><span class="hide-menu">Shelter</span></a>
</li>
<li class="nav-devider"></li>
<li class="nav-small-cap">ALL ADS</li>
<li>
    <a class="" href="{{route('pet-index')}}" aria-expanded="false"><i class="fa fa-paw"></i><span class="hide-menu">Pet</span></a>
</li>
<li>
    <a class="" href="{{route('item-index')}}" aria-expanded="false"><i class="fa fa-briefcase"></i><span class="hide-menu">Item</span></a>
</li>
<li>
    <a class="" href="{{route('service-index')}}" aria-expanded="false"><i class="fa fa-gears "></i><span class="hide-menu">Service</span></a>
</li>
@else
<li class="nav-devider"></li>
<li class="nav-small-cap">COMMUNITY</li>
<li>
    <a class="" href="{{route('shop-index')}}" aria-expanded="false"><i class="fa fa-shopping-cart"></i><span class="hide-menu">Shop</span></a>
</li>
<li>
    <a class="" href="{{route('clinic-index')}}" aria-expanded="false"><i class="fa fa-plus-square"></i><span class="hide-menu">Clinic</span></a>
</li>
<li>
    <a class="" href="{{route('shelter-index')}}" aria-expanded="false"><i class="fa fa-home"></i><span class="hide-menu">Shelter</span></a>
</li>
<li class="nav-devider"></li>
<li class="nav-small-cap">MY ADS</li>
<li>
    <a class="" href="{{route('pet-index')}}" aria-expanded="false"><i class="fa fa-paw"></i><span class="hide-menu">Pet</span></a>
</li>
<li>
    <a class="" href="{{route('item-index')}}" aria-expanded="false"><i class="fa  fa-briefcase"></i><span class="hide-menu">Item</span></a>
</li>
<li>
    <a class="" href="{{route('service-index')}}" aria-expanded="false"><i class="fa fa-gears "></i><span class="hide-menu">Service</span></a>
</li>


@endif
