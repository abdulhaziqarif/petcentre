@extends('master')

@section('body')
<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-default">
            <a class="box bg-info text-center" href="{{route('user-index')}}">
                <h1 class="font-light text-white">{{ $user }}</h1>
                <h6 class="text-white">Users</h6>
            </a>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-primary card-inverse">
            <a class="box text-center" href="{{route('shop-index')}}">
                <h1 class="font-light text-white">{{ $shop }}</h1>
                <h6 class="text-white">Shop</h6>
            </a>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-success">
            <a class="box text-center" href="{{route('clinic-index')}}">
                <h1 class="font-light text-white">{{ $clinic }}</h1>
                <h6 class="text-white">Clinics</h6>
            </a>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-warning">
            <a class="box text-center" href="{{route('shelter-index')}}">
                <h1 class="font-light text-white">{{ $shelter }}</h1>
                <h6 class="text-white">Shelters</h6>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 ">
        <div class="card">
        <div class="card-body">
            <h4 class="card-title" style="padding: 10px;">Latest Registration</h4>
            <div class="message-box">
                <div class="message-widget message-scroll">
                    @foreach ($recent_user as $recent)
                    <!-- Message -->
                    @if($recent->status == 1)
                    <a href="{{route('profile',$recent->id)}}">
                    @else
                    <a href="#">
                    @endif
                        <div class="user-img"> <img src="{{$recent->photo_url}}" alt="user" class="img-circle"></div>
                        <div class="mail-contnet">
                            <h5>{{ $recent->username }}</h5> 
                            <span class="mail-desc">
                                <b>{{ $recent->username }}</b> 
                                @if($recent->status == 0)
                                waiting to active their account
                                @else
                                have just successfully registered 
                                @endif
                            </span> 
                                <span class="time"><?php echo time_elapsed_string($recent->created_at); ?> 
                                    @if($recent->roles == 'seller')
                                    <span class="badge badge-info">seller</span>
                                    @elseif($recent->roles == 'shop')
                                    <span class="badge badge-primary">shop</span>
                                    @elseif($recent->roles == 'clinic')
                                    <span class="badge badge-success">clinic</span>
                                    @elseif($recent->roles == 'shelter')
                                    <span class="badge badge-warning">shelter</span>
                                    @endif

                                    @if($recent->review == 1)
                                    <span class="badge badge-success">verified</span>
                                    @else
                                    <span class="badge badge-default">not verify yet</span>
                                    @endif
                                </span> 
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="row">
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-default">
            <a class="box text-center" style="background-color: #f5cd79; " href="{{route('pet-index')}}"> 
                <h1 class="font-light text-white">{{ $pet }}</h1>
                <h6 class="text-white">Pet</h6>
            </a>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-primary card-inverse">
            <a class="box text-center" style="background-color: #3dc1d3; " href="{{route('item-index')}}">
                <h1 class="font-light text-white">{{ $item }}</h1>
                <h6 class="text-white">Item</h6>
            </a>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-success">
            <a class="box text-center" style="background-color: #cf6a87; " href="{{route('service-index')}}">
                <h1 class="font-light text-white">{{ $service }}</h1>
                <h6 class="text-white">Service</h6>
            </a>
        </div>
    </div>
    <!-- Column -->
    <div class="col-md-6 col-lg-3 col-xlg-3">
        <div class="card card-inverse card-warning">
            <a class="box text-center" style="background-color: #574b90; " href="{{route('user-index')}}">
                <h1 class="font-light text-white">{{ $active_user }}</h1>
                <h6 class="text-white">Active User</h6>
            </a>
        </div>
    </div>
</div>
<?php 
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
} ?>
@stop