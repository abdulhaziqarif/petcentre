<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder {

	public function run()
	{
		//DB::table('users')->delete();

		// admin
		User::create(array(
				'email' => 'admin@pethub.com',
				'username' => 'admin',
				'password' => Hash::make('admi123'),
				'roles' => 'admin'
			));
	}
}