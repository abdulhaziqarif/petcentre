<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewsTable extends Migration {

	public function up()
	{
		Schema::create('news', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->text('post');
			$table->string('user_id');
			$table->string('photo_url');
		});
	}

	public function down()
	{
		Schema::drop('news');
	}
}