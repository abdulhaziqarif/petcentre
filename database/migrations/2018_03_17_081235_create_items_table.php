<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateItemsTable extends Migration {

	public function up()
	{
		Schema::create('items', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->float('price');
			$table->integer('user_id');
			$table->string('description');
		});
	}

	public function down()
	{
		Schema::drop('items');
	}
}