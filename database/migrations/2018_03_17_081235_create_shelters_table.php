<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSheltersTable extends Migration {

	public function up()
	{
		Schema::create('shelters', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('address');
			$table->string('phone');
			$table->string('user_id');
		});
	}

	public function down()
	{
		Schema::drop('shelters');
	}
}