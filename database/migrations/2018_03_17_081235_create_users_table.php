<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('email')->unique();
			$table->string('username');
			$table->string('password');
			$table->string('photo_url')->nullable();
			$table->string('roles');
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}