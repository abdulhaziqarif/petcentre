<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePetsTable extends Migration {

	public function up()
	{
		Schema::create('pets', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->integer('user_id');
			$table->float('price');
			$table->string('description');
		});
	}

	public function down()
	{
		Schema::drop('pets');
	}
}