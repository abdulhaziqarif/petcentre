<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfileTable extends Migration {

	public function up()
	{
		Schema::create('profile', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('address');
			$table->string('phone');
			$table->string('user_id');
		});
	}

	public function down()
	{
		Schema::drop('profile');
	}
}